//
//  CoreDataManager.swift
//  LoveCRUD
//
//  Created by Oktavia Citra on 26/05/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager {
    init() {
        
    }
    
    func appContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        return managedContext
    }
    
    func createObject(managedContext: NSManagedObjectContext, entityName: String) -> NSManagedObject {
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedContext)!
        let object = NSManagedObject(entity: entity, insertInto: managedContext)
        return object
    }
    
    func saveData(managedContext: NSManagedObjectContext) {
        do {
          try managedContext.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
    }
}
