//
//  ViewController.swift
//  LoveCRUD
//
//  Created by Oktavia Citra on 26/05/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var loves: [NSManagedObject] = []
    var coreDataManager = CoreDataManager()
    var isSearchBarEmpty: Bool {
      return self.navigationItem.searchController!.searchBar.text?.isEmpty ?? true
    }
    var filteredLoves: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTable()
        setUpNavigation()
    }
    
    func setUpTable() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .always
        self.navigationItem.searchController = UISearchController(searchResultsController: nil)
        self.navigationItem.searchController!.searchResultsUpdater = self
        self.navigationItem.searchController!.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let managedContext = coreDataManager.appContext()
        read(managedContext: managedContext)
        tableView.reloadData()
    }
    
    @IBAction func cancel(segue:UIStoryboardSegue) {
       
    }

    @IBAction func done(segue:UIStoryboardSegue) {
        let loveViewController = segue.source as! LoveViewController
        
        let managedContext = coreDataManager.appContext()
        if loveViewController.mode == "edit" {
            update(loveViewController: loveViewController, managedContext: managedContext)
        }else {
            create(loveViewController: loveViewController, managedContext: managedContext)
        }
        coreDataManager.saveData(managedContext: managedContext)
        
        tableView.reloadData()
    }
    
    func create(loveViewController: LoveViewController, managedContext: NSManagedObjectContext) {
        let love: NSManagedObject = coreDataManager.createObject(managedContext: managedContext, entityName: "Love")
        love.setValue(loveViewController.name, forKeyPath: "name")
        love.setValue(loveViewController.phoneNumber, forKeyPath: "phoneNumber")
        loves.append(love)
    }
    
    func read(managedContext: NSManagedObjectContext) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Love")
        do {
            loves = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch!\n \(error), \(error.userInfo)")
        }
    }
    
    func update(loveViewController: LoveViewController, managedContext: NSManagedObjectContext) {
        let love: NSManagedObject = managedContext.object(with: loves[loveViewController.id!].objectID)
        loveViewController.mode = ""
        love.setValue("\(loveViewController.name!)", forKeyPath: "name")
        love.setValue("\(loveViewController.phoneNumber!)", forKeyPath: "phoneNumber")
        loves[loveViewController.id!] = love
    }
    
    func delete(indexPath: IndexPath) {
        let managedContext = coreDataManager.appContext()
        managedContext.delete(loves[indexPath.row] as NSManagedObject)
        coreDataManager.saveData(managedContext: managedContext)

        loves.remove(at: indexPath.row)

        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        tableView.endUpdates()
    }
    
    func moveScreen(indexPath: IndexPath) {
        let loveViewController = storyboard?.instantiateViewController(withIdentifier: "LoveViewController") as! LoveViewController
        
        loveViewController.mode = "edit"
        loveViewController.id = indexPath.row
        
        let love: NSManagedObject = loves[indexPath.row]
        loveViewController.name = love.value(forKeyPath: "name") as? String
        loveViewController.phoneNumber = love.value(forKeyPath: "phoneNumber") as? String
        
        self.present(loveViewController, animated: true, completion: nil)
    }
    
    func search(searchText: String) {
        let managedContext = coreDataManager.appContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Love")
        let predicate: NSPredicate = NSPredicate(format: "name contains[c] %@", searchText)
        fetchRequest.predicate = predicate
        do {
            filteredLoves = try managedContext.fetch(fetchRequest)
        }catch {
            print(error.localizedDescription)
        }
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredLoves.count > 0 {
            return filteredLoves.count
        } else if filteredLoves.count == 0 && !isSearchBarEmpty {
            return 0
        } else {
            return loves.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
        let love: NSManagedObject = filteredLoves.count > 0 ? filteredLoves[indexPath.row] : loves[indexPath.row]
        cell.textLabel?.text = love.value(forKeyPath: "name") as? String
        cell.detailTextLabel?.text = love.value(forKeyPath: "phoneNumber") as? String
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") {
            (action, view, nil) in self.delete(indexPath: indexPath)
        }
        
        let edit = UIContextualAction(style: .normal, title: "Edit") {
            (action, view, nil) in self.moveScreen(indexPath: indexPath)
        }
        
        return UISwipeActionsConfiguration (actions: [delete, edit])
    }
}

extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if isSearchBarEmpty {
            filteredLoves.removeAll()
        } else  {
            search(searchText: searchController.searchBar.text!.lowercased())
        }
        tableView.reloadData()
    }

}


