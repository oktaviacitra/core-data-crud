## Features
- create
- read
- update
- delete
- search

## Component
- UITableView
- SearchNavigationController

## Library
- UIKit
- Foundation
- CoreData